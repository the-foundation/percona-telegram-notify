#!/bin/bash

send_tgwebhook() {
# With the release of version 3, Webhook2Telegram was rebranded to Telepush 
# Recipient token is now encoded as a path parameter, instead of included inside the payload. Routes have changed, e.g. from /api/messages to /api/messages/<recipient>, while the recipient_token field was removed from a message's JSON schema

[[ ! -z "${TGWEBBHOOKURL}" ]] && [[ ! -z "${TGWEBBHOOKTOK}" ]] && [[ ! -z "$2" ]] && [[ ! -z "$1" ]] && {
#echo " sys.log   | LOGGER_TELEGRAM_FOR $PREFIX sending .. "$(echo "$2"|wc -c )" byte" to ${TGWEBBHOOKURL}

    verify="-k"
    [[ "VERIFY_SSL" = "true" ]] && verify=""
    tgtxt="$2"
    tgtxt=${tgtxt//\'/}
    tgtxt=${tgtxt//\"/}

test -e /dev/shm/.DEBUG.TG &>/dev/null && ( echo '{
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' > /dev/shm/.DEBUG.TG.json ; chmod go-rw /dev/shm/.DEBUG.TG.json
) &

OUTFILE=/dev/shm/.tgperconawebhook.out.log
LOGFILE=/dev/shm/.tgperconawebhook.log
echo > "$OUTFILE"

##txt

## v3 ( telepush)
curl_code=$(
echo '{
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' | curl --fail -Lv $verify -X POST ${TGWEBBHOOKURL}/${TGWEBBHOOKTOK} -H "Content-Type: application/json" --data-binary @/dev/stdin -o "$OUTFILE" -w "%{http_code}" 2>$OUTFILE  )

echo "$curl_code"|grep -q 202 ||  (
## v2 ( webhook2telegram)
echo '{
"recipient_token": "'${TGWEBBHOOKTOK}'",
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' | curl --fail -Lv $verify -X POST ${TGWEBBHOOKURL}                  -H "Content-Type: application/json" --data-binary @/dev/stdin  -o "$OUTFILE"  2>$OUTFILE 

) ;

chmod go-rwx "$OUTFILE" "$LOGFILE"

echo -n ; } ; ## end params TGWEBBHOOK(URL/TOK) found
echo -n ; } ; # end send_tg_webhook

while [[ "$#" -gt 0 ]]; do case $1 in
  -m|--members) members="$2"; shift;;
  -s|--status)  status="$2"; shift;;
#  *) echo "Unknown parameter passed: $1"; exit 1;;
esac; shift; done

MSGTXT="PERCONA-CHANGE:: $(hostname) || STATUS: $status "

##MEMBER LIST IFS is , so make it html break
if [[ ! -z "$members" ]];then MSGTXT=${MSGTXT}" || MEMBERS: <br>|| "${members//,/<br>||};fi

##REFORMAT OUR DOUBLE PIPES
MSGTXT="${MSGTXT//||/<br>||}"
MSGTXT=$(echo $MSGTXT |sed 's/<br>/\n/g')

##telegram-notify dislikes underscores ( in markdown it is italic )
MSGTXT="${MSGTXT//_/-}"

test -e /dev/shm/.DEBUG.TG &>/dev/null &&  ( echo $MSGTXT > /dev/shm/.lastperconamsg ) & 
test -e /etc/pam-tg-webhook.conf  && CONFFILE=/etc/pam-tg-webhook.conf
test -e /etc/percona-tg-webhook.conf  && CONFFILE=/etc/percona-tg-webhook.conf
[[ -z "$CONFFILE" ]] ||  {  source "$CONFFILE" ; res=$( send_tgwebhook "AUTH@$(hostname -f) / $(hostname)" "$MSGTXT" 2>&1 );echo "$res"|grep -i "error" && exit 69 ;echo "$res"|grep -i "error" || exit 0 ; } ;

test -e /etc/telegram-notify-percona.conf &&  {
  which telegram-notify || exit 44
#echo $status|grep -q ynced && (echo "$MSGTXT" | telegram-notify --success --text -)  || ( echo "$MSGTXT" | telegram-notify --warning --text - )

        echo $status|grep -q "ynced"  && { echo  "$MSGTXT" | /usr/bin/telegram-notify --config /etc/telegram-notify-percona.conf --silent  --warning --text -  ; exit 0 ; } ;
##|grep '\[Error\]' || exit 0 ; } ;
       echo $status|grep -q "ynced"   || { echo  "$MSGTXT" | /usr/bin/telegram-notify --config /etc/telegram-notify-percona.conf --silent --text -             ; exit 0 ; } ##|grep '\[Error\]' || exit 0 ; } ;
echo -n ; } ;
###fork end
#) &